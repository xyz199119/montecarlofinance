%Option Price Output
set(0,'defaultaxesfontsize',24,'defaulttextfontsize',24) %make font larger
set(0,'defaultLineLineWidth',3) %thick lines
set(0,'defaultTextInterpreter','latex') %latex axis labels
set(0,'defaultLineMarkerSize',40) %latex axis labels

%% Output numerical results
disp('For a stock modeled by the geometric Brownian motion')
if isfield(pay_param,'control')
   if numel(pay_param.control)>0
      disp(['   using the <strong>control variate</strong> ' cvname])
   end
end
if isfield(path_param,'meanshift')
   if path_param.meanshift
      disp(['   using a <strong>shifted mean</strong> of ' ...
         num2str(path_param.drift)])
   end
end
if isfield(path_param,'anti')
   if path_param.anti
      disp('   using <strong>antithetic variates</strong>')
   end
end
disp(['initial stock price = $' num2str(path_param.S0)])
disp(['       time horizon = ' num2str(path_param.T) ' years'])
disp(['      interest rate = ' num2str(100*path_param.r) '%'])
disp(['         volatility = ' num2str(100*path_param.sig) '%'])
disp(['For the <strong>' name '</strong> Option'])
if strncmp(pay_param.paytype,'look',4)==false
   disp(['       strike price = $' num2str(pay_param.K)])
end
if strncmp(pay_param.paytype,'upin',4)
   disp(['            barrier = $' num2str(pay_param.barrier)])
end
disp(['       # time steps = ' num2str(path_param.d)])
disp(['  approximate price = $' num2str(price)])
disp(['          tolerance = $' num2str(out_param.abstol)])
disp(['        uncertainty = ' num2str(100*out_param.alpha) '%'])
disp(['      total samples = ' num2str(out_param.n)])
disp(['       time elapsed = ' num2str(out_param.time) ' seconds' char(10)])

